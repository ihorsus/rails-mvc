# Homework #6: Rails VC of MVC and Feature Testing

Let's imagine yourself as a developer at the very beggining of the startup called twitter. Your colleague's computer has been attacked by hackers. Some parts of the project have been removed. Since he really loved that project, he managed to restore some of that parts but not all of it. That is great, since he restored user's sign in/sign up flow and tweeting functionality, but lost everything else...
You have to help him to recover all of the missing features.
So let's start to work, because at Sunday's noon there should be a presentation for investors and the project of your life is under a huge risk...

So, what needs to be done?

You have a functionality that your colleague have done by himself, and also you have some feature tests for the functionality that should be done by yourself.

## Task description:

1. Add functionality for creating and deleting comments under tweets.

**Tip 1**: Start with creating appropriate model and integrating it into existing codebase. Don't forget about validations and unit testing here. Then proceed with controllers and views.

**Tip 2** have a look on feature tests that were left for you, you can get important insights for your implementation from them :) (spec/features/user_commenting_tweet_spec.rb)

2. Add functionality for liking tweets and liking comments

3. Add integration tests for tweets creating / deleting and liking.

### Task acceptance criteria:
* `0` - Functionality for creating and deleting comments under tweets is added.
* `1` - Functionality for creating and deleting comments under tweets is added + related feature tests and specs are covering creating and deleting comments. Functionality for liking tweets and comments are added. Tests for liking can be red.
* `2` - Functionality for creating and deleting comments under tweets is added + related feature tests and specs are covering creating and deleting. Functionality for liking tweets and comments are added + feature tests are covering liking tweets and liking comments.

### How to setup project

1. Fork the Rails MVC  repository as explained in the [instructions](https://gitlab.com/pivorak-rsc-2019/homeworks/materials-and-instructions).
2. `gem install bundler`
3. `bundle install`
4. `bundle exec rails db:setup`
5. `bundle exec rspec spec`
6. Tests should be failing. This is okay. This is what will guide you!
7. `bundle exec rails s`
8. Visit http://localhost:3000/users/sign_up
9. Sign up as user
10. Login as registered user
You should see page with profile and tweet form: ![img](https://user-images.githubusercontent.com/5591973/84572284-9cc94f00-ada1-11ea-9d16-1937ceafb396.png)

### Useful links
- [Model / Migrations](https://guides.rubyonrails.org/active_record_migrations.html)
- [Associations](https://guides.rubyonrails.org/association_basics.html)
- [Forms](https://guides.rubyonrails.org/v5.0/form_helpers.html)
- [Routing](https://guides.rubyonrails.org/routing.html)
- [Capybara Cheatsheet](https://devhints.io/capybara)

### Useful commands
- Run features in browser:

```bash
CHROME=true rspec spec/features/user_signing_in_spec.rb
```
